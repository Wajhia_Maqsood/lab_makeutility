#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vector.h"


struct vector_data matrix_data_array1[NUM_THREADS];

void *VectorMult(void *threadarg)
{
clock_t t;

   int taskid, rows, columns;
   int *marray1;
   int *marray2;
int *result1;
   int i = 0;
   int j=0;
   struct vector_data *my_data;

   my_data = (struct vector_data *) threadarg;
   taskid = my_data->thread_id;
   rows = my_data->rows;
   columns = my_data->columns;
   marray1 = my_data->marray1;
   marray2 = my_data->marray2;
   result1 = my_data->result1;

int k =0;
t = clock();
		for (i = taskid; i < 10; i=i+2) {
			for (j = 0; j < 10; j++) {
					result1[i] += marray1[(i*10) + j] * marray2[j];
			}
		}

t = clock() - t;
double time_taken = ((double)t)/CLOCKS_PER_SEC;

/*for (i = 0; i < (10*1); i++) {
			printf("%d ",result1[i]);
				printf("\n");
		}*/
printf("Time taken: %f", time_taken);
pthread_exit(NULL);
}
